import toml
import pandas as pd
import numpy as np
import indentoolbox as itb
from indentoolbox.core import Test, Batch, Device, Operator, Sample, Step
import toml


def read_hysitron_nano(path, protocol=None, **kwargs):
    """
    Reads a test txt output in the Hysitron nano format.
    """
    lines = open(path).readlines()
    disp, force, time = [[]], [[]], [[]]
    # for pos in [0, 0, 0, 0, 1]:
    #     lines.pop(pos)
    while True:
        line = lines[0]
        words = line.split()
        if len(words != 0):
            if words[0].isnumeric():
                break
        else:
            lines.pop(0)
    for i in range(len(lines)):
        line = lines[i]
        if line.split() == []:
            disp.append([])
            force.append([])
            time.append([])
        else:
            words = line.split()
            disp[-1].append(float(words[0]) * 1.0e-9)
            force[-1].append(float(words[1]) * 1.0e-6)
            time[-1].append(float(words[2]))
    N_steps = len(time)
    if protocol != None:
        if len(protocol) != N_steps:
            print(
                f"<Warning: protocol length {len(protocol)} does not match step number {N_steps}>"
            )
    else:
        protocol = ["Step" for i in range(N_steps)]
    steps = []
    for i in range(N_steps):
        step_kind = protocol[i]
        step_data = pd.DataFrame()
        step_data["time"] = time[i]
        step_data["disp"] = disp[i]
        step_data["force"] = force[i]
        # step = step_mapping[step_kind](data=step_data)
        step_class = getattr(itb.core, step_kind)
        step = step_class(data=step_data)
        steps.append(step)
    test = Test(steps=steps, **kwargs)
    return test


def read_batch_from_folder(folder_path):
    metadata = toml.load(f"{folder_path}/metadata.toml")
    format = metadata["format"]
    file_format = format["file_format"]
    protocol = format["protocol"]

    data = {}
    tests = []
    tests_metadata = metadata["tests"]
    for test_metadata in tests_metadata:
        path = f"{folder_path}/{test_metadata['path_to_data']}"
        # if file_format == "hysitron nano":
        #     test = read_hysitron_nano(path, protocol=protocol, metadata=test_metadata)
        #     tests.append(test)
        test = Test.from_txt(path, protocol, file_format)
        tests.append(test)
    data["tests"] = tests
    data["device"] = Device(**metadata["device"])
    data["operators"] = [Operator(**md) for md in metadata["operators"]]
    data["sample"] = Sample(**metadata["sample"])
    tip_class_name = metadata["tip"]["class"]
    tip_class = getattr(itb.core, tip_class_name)
    data["tip"] = tip_class(**metadata["tip"]["attributes"])
    batch = Batch(**data)
    return batch
    """file_format = metadata["file_format"]

    tests = []
    protocol = metadata["protocol"]
    tests_metadata = metadata["tests"]
    for test_metadata in tests_metadata:
        path = f"{folder_path}/{test_metadata['path_to_data']}"
        if file_format == "hysitron nano":
            test = read_hysitron_nano(path, protocol=protocol, metadata=test_metadata)
            tests.append(test)
    tip_class_name = metadata["tip"]["class"]
    tip_class = getattr(itb.core, tip_class_name)
    tip = tip_class(**metadata["tip"]["attributes"])
    print(tip_class)
    batch = Batch(tests=tests, metadata=metadata, tip=tip)
    return batch, metadata"""

def load_dcmii(excel_file):
    """
    Load XP DCMII head results stored in Excel file export

    TODO: AB work in progress on this function

    Parameters
    ----------
    excel_file : str
        name of excel file

    Returns
    -------
    list
        each element is a indentoolbox.core.Test containing an indentation
        data

    """
    # Read all sheets in your File
    sheets = pd.read_excel(excel_file, sheet_name=None)

    # only sheets containing data, starting by Test, are kept
    sheet_names = sheets.keys()
    sheet_names = [page for page in sheet_names if page.startswith("Test")]
    sheet_names.sort()

    standard_columns = ["time", "disp", "force", "step", "test"]
    special_columns = sheets[sheet_names[0]].columns[
        4:
    ]  # exclude segment and standard columns

    columns = (
        standard_columns + list(special_columns) + ["test name"]
    )  # we keep test name to easy the life of xp users

    tests = []
    for test_number, sheet_name in enumerate(sheet_names):
        df_test = pd.DataFrame(columns=columns)

        # getting markers for segment change
        active_sheet = sheets[sheet_name]

        steps = []
        markers = active_sheet["Segment"].dropna()
        for step_n, (line, line_next) in enumerate(
            zip(markers[:-1].index, markers[1:].index)
        ):
            if active_sheet.iloc[line]["Segment"] != "END":
                sub_df = active_sheet.iloc[line:line_next]

                names_std = [
                    "Time On Sample",
                    "Displacement Into Surface",
                    "Load On Sample",
                ]
                df_to_apnd = sub_df[names_std + list(special_columns)]
                df_to_apnd.insert(3, "step", step_n)
                df_to_apnd.insert(4, "test", test_number)
                df_to_apnd.insert(len(df_to_apnd.columns), "test name", sheet_name)
                # columns are renamed according to indentoolbox standard
                df_to_apnd = df_to_apnd.rename(
                    columns={
                        "Time On Sample": "time",
                        "Displacement Into Surface": "disp",
                        "Load On Sample": "force",
                    }
                )
                # SI units conversion
                df_to_apnd["force"] = df_to_apnd["force"] * 1e-3
                df_to_apnd["disp"] = df_to_apnd["disp"] * 1e-9
                df_to_apnd["Hardness"] = df_to_apnd["Hardness"] * 1e9
                df_to_apnd["Modulus"] = df_to_apnd["Modulus"] * 1e9

                this_step = Step(df_to_apnd)
                steps.append(this_step)
            else:
                break

        this_test = Test(steps)
        tests.append(this_test)
    
    return tests
