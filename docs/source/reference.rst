==============
 API Reference
==============

.. .. _reference_db_database:



Batch class
===========

.. autoclass:: indentoolbox.core.Batch
    :members:
    :undoc-members:

Test class
===========

.. autoclass:: indentoolbox.core.Test
    :members:
    :undoc-members:

Step classes
============

.. autoclass:: indentoolbox.core.Step
    :members:
    :undoc-members:

.. autoclass:: indentoolbox.core.LoadingStep
    :members:
    :undoc-members:

.. autoclass:: indentoolbox.core.ConicalLoadingStep
    :members:
    :undoc-members:

.. autoclass:: indentoolbox.core.UnloadingStep
    :members:
    :undoc-members:


Metaclasses
===========

.. autoclass:: indentoolbox.core.Container
    :members:
    :undoc-members:

.. autoclass:: indentoolbox.core.StepVector
    :members:
    :undoc-members:

.. autoclass:: indentoolbox.core.AttrArray
    :members:
    :undoc-members:


Post-processing
===============

.. autofunction:: indentoolbox.processing.OliverPharr

.. autofunction:: indentoolbox.processing.GIAN99

.. autofunction:: indentoolbox.processing.DAO01

.. autofunction:: indentoolbox.processing.CASA05
